package Model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQLInteger implements MySQLObject {

    private Integer value = 0;

    @Override
    public void createFromMySQL(ResultSet row) throws SQLException {
        this.value = row.getInt("value");
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
