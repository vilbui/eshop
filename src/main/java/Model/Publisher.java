/**
 * @(#) Publisher.java
 */

package Model;

import java.util.ArrayList;

public class Publisher
{
    private int ID;
    
    private Address address;
    
    private ArrayList<Book> books = new ArrayList<>();
    
    private String name;

    public Publisher(int id, Address address, String name){
        this.ID = id;
        this.address = address;
        this.name = name;
    }

    public void add( )
    {
        
    }
    
    public void edit( )
    {
        
    }
    
    public void delete( )
    {
        
    }
    
    public static Publisher getOne(int ID )
    {
        return new Publisher(ID, new Address("Kauno g.", "Vilnius", "52153", 15, 2), "Šviesa");
    }
    
    public static ArrayList<Publisher> getList( )
    {
        return new ArrayList<>();
    }
    
    public static void addSeveral( )
    {
        
    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ArrayList<Book> getBooks() {
        return books;
    }

    public void setBooks(ArrayList<Book> books) {
        this.books = books;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
