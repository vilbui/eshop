/**
 * @(#) OrderState.java
 */

package Model;

public enum OrderState
{
    CREATED, PAID, SENT
}
