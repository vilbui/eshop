/**
 * @(#) Role.java
 */

package Model;

enum Role
{
    ADMINISTRATOR, SELLER
}
