/**
 * @(#) Comment.java
 */

package Model;

import java.util.ArrayList;
import java.util.Date;

public class Comment
{
    private int ID;
    
    private String text;
    
    private Book book;
    
    private User user;
    
    private Date date;

    public Comment(int ID, String text, Date date, User user){
        this.ID = ID;
        this.text = text;
        this.date = date;
        this.user = user;

    }

    public void add( )
    {
        
    }
    
    public static ArrayList<Comment> bookComments(int book_id )
    {
        ArrayList<Comment> comments = new ArrayList<>();

        comments.add(new Comment(1, "Labai įdomus komentaras apie knygą", new Date((long)1555957477), User.getOne(1)));
        comments.add(new Comment(2, "Knyga labai patiko. Siūlau pirkti visiems !", new Date((long)1552933477), User.getOne(1)));

        return comments;
    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
