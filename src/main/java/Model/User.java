/**
 * @(#) User.java
 */

package Model;

public class User
{
    protected int ID;

    protected String name;

    protected String surname;

    protected String email;

    protected Order orders;

    protected Comment comments;

    protected Rate ratings;

    protected String password;


    public User(int id, String name, String surname, String email){
        this.ID = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
    }
    
    public void add( )
    {
        
    }
    
    public void edit( )
    {
        
    }
    
    public void login( )
    {
        
    }
    
    public void logout( )
    {
        
    }
    
    public static User getOne( int ID )
    {
        return new User(ID, "Vilius", "Buividavičius", "vilius.buividavicius@ktu.edu");
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Order getOrders() {
        return orders;
    }

    public void setOrders(Order orders) {
        this.orders = orders;
    }

    public Comment getComments() {
        return comments;
    }

    public void setComments(Comment comments) {
        this.comments = comments;
    }

    public Rate getRatings() {
        return ratings;
    }

    public void setRatings(Rate ratings) {
        this.ratings = ratings;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
