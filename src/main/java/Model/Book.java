/**
 * @(#) Book.java
 */

package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Book implements MySQLObject
{
    private int ID;
    
    private String name;
    
    private String genre;
    
    private String author;
    
    private ArrayList<Comment> comments;
    
    private Publisher publisher;
    
    private Rate ratings;
    
    private Order orders;
    
    private double price;
    
    private Seller seller;
    
    private String pdf_file;
    
    public void add( )
    {
        
    }

    public Book() {
    }

    @Override
    public void createFromMySQL(ResultSet row) throws SQLException {
        ID = row.getInt("id");
        name = row.getString("name");
        genre = row.getString("genre");
        author = row.getString("author");
        price = row.getDouble("price");
        pdf_file = row.getString("pdf_file");
        publisher = Publisher.getOne(1);
    }

    public void edit( )
    {
        
    }
    
    public void delete( )
    {
        
    }

    public static Book getOne(Integer id){
        Book book = null;
        try{
            PreparedStatement statement = MySQL.getConnection().prepareStatement("SELECT * FROM " + MySQL.TBL_BOOKS + " WHERE id = ?");
            statement.setInt(1, id);
            book = MySQL.SelectOne(statement, Book.class);
        } catch(SQLException e){
            System.out.println(e.toString());
        }

        return book;
    }


    public static ArrayList<Book> getList(){
        try {
            PreparedStatement statement = MySQL.getConnection().prepareStatement("SELECT * FROM " + MySQL.TBL_BOOKS);
            return MySQL.Select(statement, Book.class);
        } catch (SQLException e) {
            System.out.println(e.toString());
        }

        return null;
    }
    
    public static ArrayList<Book> getOrderBooks(Order order)
    {
        if(order == null){
            return new ArrayList<>();
        }
        try {
            PreparedStatement statement = MySQL.getConnection().prepareStatement("SELECT b.* FROM " + MySQL.TBL_BOOKS + " as b RIGHT JOIN " + MySQL.TBL_ORDER_BOOKS + " as ob ON ob.book = b.id WHERE ob.order = ?");
            statement.setInt(1, order.getID());
            return MySQL.Select(statement, Book.class);
        } catch (SQLException e) {
            System.out.println(e.toString());
        }

        return null;
    }



    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public Rate getRatings() {
        return ratings;
    }

    public void setRatings(Rate ratings) {
        this.ratings = ratings;
    }

    public Order getOrders() {
        return orders;
    }

    public void setOrders(Order orders) {
        this.orders = orders;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public String getPdf_file() {
        return pdf_file;
    }

    public void setPdf_file(String pdf_file) {
        this.pdf_file = pdf_file;
    }
}
