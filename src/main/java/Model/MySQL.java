package Model;

import javax.management.Query;
import java.sql.*;
import java.util.ArrayList;

public class MySQL {
    public static final String TBL_BOOKS = "books";
    public static final String TBL_ORDERS = "orders";
    public static final String TBL_ORDER_BOOKS = "order_books";


    //Config
    private static Connection connection = null;
    private static final String DBUSER = "root";
    private static final String DBHOST = "localhost";
    private static final String DBPORT = "3306";
    private static final String DBPASS = "";
    private static final String DB = "knyguparduotuve";

    public static Connection getConnection(){
        if(connection != null){
            try{
                if(connection.isClosed()){
                    connection = null;
                }
            }  catch (SQLException e) {
                connection = null;
                e.printStackTrace();
            }
        }

        if(connection == null){
            try {
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection("jdbc:mysql://" + DBHOST + ":" + DBPORT + "/" + DB + "?useUnicode=true&characterEncoding=utf-8",DBUSER, DBPASS);

                connection.prepareStatement("SET NAMES 'utf8'").execute();
            } catch (ClassNotFoundException e) {
                System.out.println("Where is your MySQL JDBC Driver?");
                e.printStackTrace();
            } catch (SQLException e) {
                System.out.println("Connection Failed! Check output console");
                e.printStackTrace();
            }
        }

        return connection;
    }

    public static <T extends MySQLObject> ArrayList<T> Select(PreparedStatement query, Class<T> type) {
        try{
            ResultSet result =  query.executeQuery();

            ArrayList<T> list = new ArrayList<>();

            while(result.next()){
                T item = type.newInstance();
                item.createFromMySQL(result);
                list.add(item);
            }
            return list;

        } catch (SQLException e) {
            System.out.println(e.toString());

            return null;
        } catch (InstantiationException|IllegalAccessException e) {
            System.out.println(e.toString());
            return null;
        }
    }

    public static <T extends MySQLObject> T SelectOne(PreparedStatement query, Class<T> type){
        try{
            ResultSet result =  query.executeQuery();

            while(result.next()){
                T item = type.newInstance();
                item.createFromMySQL(result);
                return item;
            }
            return null;

        } catch (SQLException e) {
            System.out.println(e.toString());
            return null;
        }  catch (InstantiationException|IllegalAccessException e) {
            System.out.println(e.toString());
            return null;
        }
    }


    public static void Execute(PreparedStatement query){
        try{
            query.execute();

        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }

    public static Integer Insert(PreparedStatement query){
        try{
            query.executeUpdate();
            ResultSet rs = query.getGeneratedKeys();
            if(rs.next())
            {
                return rs.getInt(1);
            }

            return null;
        }  catch (SQLException e) {
            System.out.println(e.toString());
            return null;
        }
    }

    public static String getLimitString(Integer page, Integer pagesize){
        return " LIMIT " + ((page - 1) * pagesize) + ", " + pagesize;
    }
}
