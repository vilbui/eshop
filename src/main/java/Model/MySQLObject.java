package Model;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface MySQLObject {

    void createFromMySQL(ResultSet row) throws SQLException;
}
