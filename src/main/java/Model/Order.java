/**
 * @(#) Order.java
 */

package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

public class Order implements MySQLObject
{
    private int ID;

    private Date date;
    
    private OrderState state;
    
    private String email;
    
    private User user;
    
    private ArrayList<Book> books;


    public Order(){

    }

    @Override
    public void createFromMySQL(ResultSet row) throws SQLException {
        ID = row.getInt("id");
       // date = row.getD("name");
        state = OrderState.valueOf(row.getString("state"));
        email = row.getString("email");
    }
    
    public void add()
    {
        
    }
    
    public void addBook(int book_id)
    {
        try{
            PreparedStatement query = MySQL.getConnection().prepareStatement("SELECT COUNT(*) as count FROM " + MySQL.TBL_ORDER_BOOKS + " WHERE `order` = ? AND book = ?");
            query.setInt(1, getID());
            query.setInt(2, book_id);
            ResultSet result = query.executeQuery();
            result.next();
            int count = result.getInt("count");
            result.close();
            if(count > 0){
                return;
            }

            PreparedStatement iQuery = MySQL.getConnection().prepareStatement("INSERT INTO " + MySQL.TBL_ORDER_BOOKS + "(`order`, book) VALUES(?,?)");
            iQuery.setInt(1, getID());
            iQuery.setInt(2, book_id);
            MySQL.Execute(iQuery);
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }

    public static Order getOne(int ID)
    {
        Order order = null;
        try{
            PreparedStatement statement = MySQL.getConnection().prepareStatement("SELECT * FROM " + MySQL.TBL_ORDERS + " WHERE id = ?");
            statement.setInt(1, ID);
            order = MySQL.SelectOne(statement, Order.class);
        } catch(SQLException e){
            System.out.println(e.toString());
        }

        return order;
    }

    public static Order getByIDUser(int ID, User user)
    {
        Order order = null;
        try{
            PreparedStatement statement = MySQL.getConnection().prepareStatement("SELECT * FROM " + MySQL.TBL_ORDERS + " WHERE id = ? AND user = ?");
            statement.setInt(1, ID);
            statement.setInt(2, user.getID());
            order = MySQL.SelectOne(statement, Order.class);
        } catch(SQLException e){
            System.out.println(e.toString());
        }

        return order;
    }

    public static Order getCurrent(User user)
    {
        Order order = null;
        try{
            PreparedStatement statement = MySQL.getConnection().prepareStatement("SELECT * FROM " + MySQL.TBL_ORDERS + " WHERE user = ? AND state = ?");
            statement.setInt(1, user.getID());
            statement.setString(2, OrderState.CREATED.toString());
            order = MySQL.SelectOne(statement, Order.class);


            if(order == null){
                PreparedStatement iQuery = MySQL.getConnection().prepareStatement("INSERT INTO " + MySQL.TBL_ORDERS + "(state, email, user) VALUES(?,?,?)", Statement.RETURN_GENERATED_KEYS);
                iQuery.setString(1, OrderState.CREATED.toString());
                iQuery.setString(2, user.getEmail());
                iQuery.setInt(3, user.getID());
                if(MySQL.Insert(iQuery) != null){
                    return getCurrent(user);
                }
            }
            return order;

        } catch(SQLException e){
            System.out.println(e.toString());
        }


        return order;
    }


    public static ArrayList<Order> getList(User user){
        try {
            if(user == null){
                PreparedStatement statement = MySQL.getConnection().prepareStatement("SELECT * FROM " + MySQL.TBL_ORDERS);
                return MySQL.Select(statement, Order.class);
            } else {
                PreparedStatement statement = MySQL.getConnection().prepareStatement("SELECT * FROM " + MySQL.TBL_ORDERS + " WHERE user = ? AND (state = ? OR state = ?)");
                statement.setInt(1, user.getID());
                statement.setString(2, OrderState.PAID.toString());
                statement.setString(3, OrderState.SENT.toString());
                return MySQL.Select(statement, Order.class);
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }

        return null;
    }
    
    public void changeState( OrderState busena )
    {
        try {
            PreparedStatement statement = MySQL.getConnection().prepareStatement("UPDATE " + MySQL.TBL_ORDERS + " SET state = ? WHERE id = ?");
            statement.setString(1, busena.toString());
            statement.setInt(2, getID());
            MySQL.Execute(statement);
        } catch (SQLException e) {
            System.out.println(e.toString());
        }

    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public OrderState getState() {
        return state;
    }

    public void setState(OrderState state) {
        this.state = state;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Book> getBooks() {
        return books;
    }

    public void setBooks(ArrayList<Book> books) {
        this.books = books;
    }
}
