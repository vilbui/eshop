/**
 * @(#) Address.java
 */

package Model;

public class Address
{
    private String street;
    
    private String city;
    
    private String zip;
    
    private int house_no;
    
    private int appartment_no;
    
    private Publisher publisher = null;

    public Address(String street, String city, String zip, int house_no, int appartment_no){
        this.street = street;
        this.city = city;
        this.zip = zip;
        this.house_no = house_no;
        this.appartment_no = appartment_no;
    }


    public void add( )
    {
        
    }
    
    public void edit( )
    {
        
    }
    
    public void delete( )
    {
        
    }
    
    public static Address getPublisher( )
    {
        return null;
    }
}
