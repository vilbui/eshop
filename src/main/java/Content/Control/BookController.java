/**
 * @(#) BookController.java
 */

package Content.Control;

import Content.Interface.CheckBook;
import Model.Book;
import Model.Comment;
import Model.Rate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class BookController
{
    public void add( )
    {
        
    }
    
    public void delete( )
    {
        
    }
    
    public void edit( )
    {
        
    }
    
    public static ArrayList<Book> getList( )
    {
        return Book.getList();
    }
    
    public void getOne( )
    {
        
    }
    
    public void validate( )
    {
        
    }
    
    public void openBookList( )
    {
        
    }
    
    public void openBookForm( )
    {
        
    }

    public static void checkBook(int id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Book book = Book.getOne(id);
        book.setComments(Comment.bookComments(id));

        request.setAttribute("book", book);
        request.setAttribute("book_rate", Rate.bookRate(id));

        CheckBook.open(request, response);
    }
    
    public void rateBook( )
    {
        
    }
    
    public void writeComment( )
    {
        
    }
    
    public void filter( )
    {
        
    }
    
    public void addToOrder( )
    {
        
    }
    
    
}
