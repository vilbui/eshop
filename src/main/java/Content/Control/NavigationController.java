/**
 * @(#) NavigationController.java
 */

package Content.Control;

import Content.Interface.BookList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class NavigationController
{
    public void getNavigation( )
    {
        
    }
    
    public void openRegistration( )
    {
        
    }
    
    public void openLogin( )
    {
        
    }
    
    public void openProfile( )
    {
        
    }
    
    public static void openOrderList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        OrderController.openOrderList(request, response);
    }
    
    public static void openOrder(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        OrderController.openOrder(request, response);
    }
    
    public void openPublishersList( )
    {
        
    }
    
    public static void openBookList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.setAttribute("books_list", BookController.getList());
        BookList.open(request, response);
    }
}
