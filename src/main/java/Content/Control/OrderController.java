/**
 * @(#) OrderController.java
 */

package Content.Control;

import Content.Interface.BookList;
import Content.Interface.CheckOrder;
import Content.Interface.OrderList;
import Content.Interface.Payment;
import Model.Book;
import Model.Order;
import Model.OrderState;
import Model.User;
import ThirdPartyControllers.CreditCardController;
import ThirdPartyControllers.PayseraController;
import User.Control.UserController;
import com.sun.org.apache.xpath.internal.operations.Or;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class OrderController
{
    public void getUser( )
    {
        
    }
    
    public void getUserFormatted( )
    {
        
    }
    
    public static void addBook(int book_id)
    {
        User user = UserController.getLoggedIn();
        Order order = Order.getCurrent(user);
        order.addBook(book_id);
    }
    
    public static void order(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Payment.open(request, response);
    }
    
    public static void sendBookToEmail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        User user = UserController.getLoggedIn();
        Order order = Order.getCurrent(user);

        ArrayList<Book> books = Book.getOrderBooks(order);

        sendBooks(books, order.getEmail());

        if(order.getState() != OrderState.SENT)
            order.changeState(OrderState.SENT);

        request.setAttribute("message_sent", "Knygos išsiųstos");
        request.setAttribute("order_list", Order.getList(user));
        OrderList.open(request, response);
    }
    
    public static void payseraQuery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        User user = UserController.getLoggedIn();
        Order order = Order.getCurrent(user);
        if(!PayseraController.APICall()){
            request.setAttribute("message", "Apmokėjimas nesėkmingas");
            Payment.open(request, response);
            return;
        }
        order.changeState(OrderState.PAID);
    }
    
    public static void payByCredit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        User user = UserController.getLoggedIn();
        Order order = Order.getCurrent(user);
        if(!CreditCardController.PaymentCall()){
            request.setAttribute("message", "Apmokėjimas nesėkmingas");
            Payment.open(request, response);
            return;
        }

        order.changeState(OrderState.PAID);
    }
    
    private static void sendBooks(ArrayList<Book> books, String email)
    {
        //@todo: Send book list realization
        for(Book book : books){
            book.getPdf_file(); //Append PDF files to email
        }

        //Send email
    }

    public void checkOrder( )
    {

    }

    public static Order getOrderData(int ID, User user )
    {
        return Order.getByIDUser(ID, user);
    }
    
    public static void openOrderList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        User user = UserController.getLoggedIn();
        ArrayList<Order> orders = Order.getList(user);

        request.setAttribute("order_list", orders);
        OrderList.open(request, response);
    }
    
    public static void openOrder(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        User user = UserController.getLoggedIn();

        Order order = Order.getCurrent(user);
        ArrayList<Book> books = Book.getOrderBooks(order);

        request.setAttribute("order", order);
        request.setAttribute("order_books", books);

        CheckOrder.open(request, response);
    }
}
