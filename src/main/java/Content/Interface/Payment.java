/**
 * @(#) Payment.java
 */

package Content.Interface;

import Content.Control.OrderController;
import Model.Order;
import Model.OrderState;
import Model.User;
import User.Control.UserController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Payment
{
    public static void open(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        RequestDispatcher dis=request.getRequestDispatcher("payment.jsp");
        dis.forward(request, response);
    }
    
    public static void payByPaysera(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        OrderController.payseraQuery(request, response);




        OrderController.sendBookToEmail(request, response);
    }
    
    public static void payByCredit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        OrderController.payByCredit(request, response);

        OrderController.sendBookToEmail(request, response);
    }
}
