/**
 * @(#) Navigation.java
 */

package Content.Interface;

import Content.Control.NavigationController;
import Model.Book;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Navigation
{
    public void open( )
    {
        
    }
    
    public void openRegistration( )
    {
        
    }
    
    public void openLogin( )
    {
        
    }
    
    public void openProfile( )
    {
        
    }

    public static void openOrderList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        NavigationController.openOrderList(request, response);
    }

    public static void openOrder(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        NavigationController.openOrder(request, response);
    }
    
    public void openPublishersList( )
    {
        
    }

    public static void openBookList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        NavigationController.openBookList(request, response);
    }
    
    public void logout( )
    {
        
    }
}
