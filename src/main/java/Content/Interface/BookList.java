/**
 * @(#) BookList.java
 */

package Content.Interface;

import Content.Control.BookController;
import Content.Control.OrderController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BookList
{
    public static void open(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        RequestDispatcher dis=request.getRequestDispatcher("books.jsp");
        dis.forward(request, response);
    }

    public static void checkBook(int id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        BookController.checkBook(id, request, response);
    }
    
    public void search( )
    {
        
    }
    
    public void edit( )
    {
        
    }
    
    public void delete( )
    {
        
    }
    
    public void createNew( )
    {
        
    }
    
    public static void addToOrder(int book_id)
    {
        OrderController.addBook(book_id);
    }
    
    public void confirm( )
    {
        
    }
}
