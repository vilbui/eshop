/**
 * @(#) CheckBook.java
 */

package Content.Interface;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CheckBook
{
    public static void open(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        RequestDispatcher dis=request.getRequestDispatcher("book.jsp");
        dis.forward(request, response);
    }
    
    public void edit( )
    {
        
    }
    
    public void delete( )
    {
        
    }
    
    public void addToOrder( )
    {
        
    }
    
    public void rate( )
    {
        
    }
    
    public void addComment( )
    {
        
    }
    
    
}
