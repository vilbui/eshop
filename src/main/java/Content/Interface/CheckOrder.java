/**
 * @(#) CheckOrder.java
 */

package Content.Interface;

import Content.Control.OrderController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CheckOrder
{
    public static void open(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        RequestDispatcher dis=request.getRequestDispatcher("order.jsp");
        dis.forward(request, response);
    }
    
    public void deleteBook( )
    {
        
    }
    
    public void checkBook( )
    {
        
    }
    
    public static void order(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        OrderController.order(request, response);
    }
    
    public void sendBookToEmail( )
    {
        
    }
    
    
}
