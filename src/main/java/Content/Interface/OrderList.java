/**
 * @(#) OrderList.java
 */

package Content.Interface;

import Content.Control.OrderController;
import Model.Order;
import Model.User;
import User.Control.UserController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class OrderList
{
    public static void open(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        RequestDispatcher dis=request.getRequestDispatcher("orderlist.jsp");
        dis.forward(request, response);
    }
    
    public void checkBook( )
    {
        
    }
    
    public static void sendBooksToEmail(int order_id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        OrderController.sendBookToEmail(request, response);
    }
}
