<%@ page import="Model.Book" %>
<%
    Book book = (Book) request.getAttribute("book");
    Float rate = (Float) request.getAttribute("book_rate");
%>
<%@ page import="Model.Comment" %>
<!DOCTYPE html>
<html>
<head>
    <title><%=book.getName()%>%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <link href="styles/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<h2>Books List</h2>
<div class="wrapper">
    <jsp:include page="navigation.jsp"/>

    <!-- Page content -->
    <div class="main">
        <div class="container">
            <div class="row book-row">
                <div class="col-sm-6">
                    <b><%=book.getName()%></b> (<%=book.getGenre()%>)<br/>
                    <%=book.getAuthor()%> (<%=book.getPublisher().getName()%>)
                </div>
                <div class="col-sm-6">
                    Knygos įvertinimas <%=rate%>/10
                </div>
            </div>

            <h2>Komentarai</h2>

            <div>
                <div class="form-group shadow-textarea">
                    <label for="exampleFormControlTextarea6">Rašyti komentarą(///Neįgyvendinta///)</label>
                    <textarea class="form-control z-depth-1" id="exampleFormControlTextarea6" rows="3" placeholder="Įveskite komentarą ..."></textarea>
                    <button class="btn btn-primary" type="button">Skelbti</button>
                </div>
                <%
                    for (Comment comment : book.getComments()) {
                %>
                <div class="row">
                    <div class="col-sm-8 book-row">
                        <span style="font-size: 12px;"><%=comment.getUser().getName()%> <%=comment.getUser().getSurname()%></span><br/>
                        <%=comment.getText()%>
                    </div>
                </div>
                <%
                    }
                %>
            </div>
        </div>
    </div>
</div>
</body>
</html>
